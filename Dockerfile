FROM hepdock/root:6.34.02-ubuntu24.04

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# APT Package Manager + LibTorch
COPY Dockerfile.packages packages
RUN apt-get update -qq \
    && ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && apt-get -y install $(awk -F '#' '{print $1}' packages) \
    && apt-get autoremove -y && apt-get clean -y \
    && rm -rf /var/cache/apt/archives/* && rm -rf /var/lib/apt/lists/*

# Conda Environment Manager
ENV PATH=/opt/conda/bin:$PATH
COPY requirements.txt /tmp/requirements.txt
RUN SHFILE="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" \
    && wget -O conda.sh $SHFILE && sh ./conda.sh -b -p "/opt/conda" && rm conda.sh

# ROOT+ and LibTorch Installation
ENV Torch_VERSION="2.5.1"
ENV CLING_MODULEMAP_FILES="/usr/local/lib/RIOPlus.modulemap:$CLING_MODULEMAP_FILES"
RUN git clone --recursive https://git.ligo.org/kagra/libraries-addons/root/root-plus.git /opt/root-plus \
    && mkdir -p /opt/root-plus/build && cd /opt/root-plus/build \
    && ../cmake/download_libtorch ${Torch_VERSION} && rsync -avu libtorch/* /usr/local \
    && cmake .. && make -j$(nproc) && make install \
    && rm -rf /opt/root-plus

# Download and install FrameL library
RUN git clone https://git.ligo.org/marco.meyer/Fr.git /opt/framel-src \
    && mkdir -p /opt/framel-src/build && cd /opt/framel-src/build \
    && git checkout fix-is-constant-evaluated \
    && cmake .. && make -j${NPROC} && make -j${NPROC} install \
    && rm -rf /opt/framel-src

WORKDIR /root
COPY .condarc .
COPY .bashrc .
COPY .env .

CMD ["bash"]