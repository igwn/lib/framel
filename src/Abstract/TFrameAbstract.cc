/**
 **********************************************
 *
 * \file TFrameAbstract.cc
 * \brief Source code of the TFrameAbstract class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "FrameFormat/Abstract/TFrameAbstract.h"
ClassImp(FrameFormat::TFrameAbstract)

#include "FrameFormat/Impl/TFrameHeader.h"

namespace FrameFormat {

    Verbosity TFrameAbstract::verbosity = Verbosity::NORMAL;
    void TFrameAbstract::SetVerbosity(Verbosity verbosity)
    {
        this->verbosity = verbosity;
    }

    Verbosity TFrameAbstract::GetVerbosity()
    {
        return this->verbosity;
    }

    bool TFrameAbstract::Verbose(Verbosity verbosity)
    {
        return this->verbosity >= verbosity;
    }

    void TFrameAbstract::SetDebug(int debugLvl)
    {
        // mutex might be implemented here
        FrLibSetLvlT(::ROOT::IsImplicitMTEnabled() ? 0 : debugLvl);
    }

    char *TFrameAbstract::Version()
    {
        return FrLibVersionF();
    }
};