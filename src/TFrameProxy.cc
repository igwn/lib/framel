/**
 **********************************************
 *
 * \file TFrameProxy.cc
 * \brief Source code of the TFrameProxy class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "FrameFormat/TFrameProxy.h"
#include "FrameFormat/TFrame.h"

ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrAdcData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrDetector>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrEvent>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrHistory>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrMsg>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrProcData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrRawData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrSerData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrSimData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrSimEvent>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrStatData>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrSummary>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrTable>)
ClassImp(FrameFormat::TFrameProxy<FrameFormat::TFrVect>)

#include "FrameFormat/Impl/TFrameHeader.h"

/** @cond */
const char *FrameFormat::TFrameProxyAbstract::DEFAULT_BRANCHNAME = "payload";
/** @endcond */

namespace FrameFormat {

    TH1* TFrameProxyAbstract::Histogram(TTree *tree, TString branchName, Option_t *option)
    {
        TString opt = option;
                opt.ToLower();

        if(tree == NULL) return NULL;

        TString dataPath = branchName+".data";
        if(tree->GetListOfLeaves()->FindObject(dataPath) == NULL) {

            TPrint::Warning(__METHOD_NAME__, "No branch \""+branchName+"\" with \"data\" found in \""+TString(tree->GetName())+"\".");    
            return NULL;
        }

        std::vector<double> data = TVarexp(branchName+".data").GetSelection(tree)[0];
        if ( data.size() < 1 ) return NULL;

        std::vector<std::vector<double>> meta = TVarexp(branchName+".startX:"+branchName+".dx:"+branchName+".nx:GTimeS:GTimeN").GetSelection(tree);

        double startX = meta[0].size() ? meta[0][0] : NAN;
        double dx     = meta[1].size() ? meta[1][0] : NAN;
        int    nx     = meta[2].size() ? meta[2][0] : 0;
        double GTimeS = meta[3].size() ? meta[3][0] : NAN;
        double GTimeN = meta[4].size() ? meta[4][0] : NAN;

        double GPS = gClockGPS->Timestamp(GTimeS, GTimeN) + startX;
        
        std::vector<double> axis;
        for(int i = 0; i < nx+1; i++) 
            axis.push_back(GPS + i*dx);

        TString hName = "h"+ROOT::IOPlus::Helpers::Ucfirst(tree->GetName());
        if(opt.Contains("delete")) {

            TObject *obj = NULL;
            gDirectory->GetObject(hName, obj);

            if(obj) obj->Delete();
        }

        TH1 *hData = ROOT::IOPlus::Helpers::Histogram(hName, tree->GetTitle(), axis, data);
        return gClock->FormatTimeAxis(hData, "", 2, ceil(axis[0]));
    }

    template<typename T>
    TFrameProxy<T>::TFrameProxy(TFrame *frame, TString _identifier, void *_ptr): identifier(_identifier), frame(frame)
    {
        do {

            TString name      = this->ReadName(_ptr);
            TString message   = this->ReadMessage(_ptr);
            int channelGroup  = this->ReadChannelGroup(_ptr);
            int channelNumber = this->ReadChannelNumber(_ptr);
            double sampleRate = this->ReadSampleRate(_ptr);

            if(!frame->IsValidTag(name))
                continue;
            if(frame->GetChannelGroups().size() > 0 && std::find(frame->GetChannelGroups().begin(),   frame->GetChannelGroups().end(), channelGroup)  == frame->GetChannelGroups().end())
                continue;
            if(frame->GetChannelNumbers().size() > 0 && std::find(frame->GetChannelNumbers().begin(), frame->GetChannelNumbers().end(), channelNumber) == frame->GetChannelNumbers().end())
                continue;

            ptr.push_back(_ptr);
            names.push_back(name);
            messages.push_back(message);
            channelGroups.push_back(channelGroup);
            channelNumbers.push_back(channelNumber);
            sampleRates.push_back(sampleRate);

            if (std::is_same<T, TFrAdcData>::value && this->frame->GetDecimate() > 1)
                FrAdcDataDecimate((FrAdcData *) _ptr, this->frame->GetDecimate());

        } while((_ptr = this->Next(_ptr)));
    }

    template <typename T>
    void *TFrameProxy<T>::GetPointer(int basket)
    {
        if(basket < 0 || basket >= ptr.size()) return NULL;
        return ptr[basket];
    }

    template <typename T>
    int TFrameProxy<T>::GetBasketID(TString objName)
    {
        for(int i = 0, N = this->names.size(); i < N; i++) {
            
            if(this->names[i].EqualTo(objName)) return i;
        }

        return -1;
    }
             
    template <typename T>
    TString TFrameProxy<T>::GetName(int basket)
    {
        if(basket < 0 || basket >= names.size()) return "";
        return this->names[basket];
    }

    template <typename T>
    TString TFrameProxy<T>::GetMessage(int basket)
    {
        if(basket < 0 || basket >= messages.size()) return "";
        return this->messages[basket];
    }

    template <typename T>
    int TFrameProxy<T>::GetChannelGroup(int basket)
    {
        if(basket < 0 || basket >= channelGroups.size()) return -1;
        return this->channelGroups[basket];
    }

    template <typename T>
    int TFrameProxy<T>::GetChannelNumber(int basket)
    {
        if(basket < 0 || basket >= channelNumbers.size()) return -1;
        return this->channelNumbers[basket];
    }

    template <typename T>
    double TFrameProxy<T>::GetSampleRate(int basket)
    {
        if(basket < 0 || basket >= sampleRates.size()) return 0;
        return this->sampleRates[basket];
    }

    template <typename T>
    int TFrameProxy<T>::GetBasketID(void *ptr)
    {
        if(ptr == nullptr) return -1;

        int index = std::find(this->ptr.begin(), this->ptr.end(), ptr) - this->ptr.begin();
        if(index == this->ptr.size()) return -1;

        return index;
    }

    template<typename T>
    void *TFrameProxy<T>::Next(void *ptr)
    {
        if(ptr == NULL) return NULL;

        if (std::is_same<T, TFrAdcData>::value)
            return ((FrAdcData *) ptr)->next;
        if (std::is_same<T, TFrDetector>::value)
             return ((FrDetector*) ptr)->next;
        if (std::is_same<T, TFrEvent   >::value)
             return ((FrEvent   *) ptr)->next;
        if (std::is_same<T, TFrHistory >::value)
             return ((FrHistory *) ptr)->next;
        if (std::is_same<T, TFrMsg     >::value)
             return ((FrMsg     *) ptr)->next;
        if (std::is_same<T, TFrProcData>::value)
             return ((FrProcData*) ptr)->next;
        if (std::is_same<T, TFrRawData >::value)
             return NULL;
        if (std::is_same<T, TFrSerData >::value)
             return ((FrSerData *) ptr)->next;
        if (std::is_same<T, TFrSimData >::value)
             return ((FrSimData *) ptr)->next;
        if (std::is_same<T, TFrSimEvent>::value)
             return ((FrSimEvent*) ptr)->next;
        if (std::is_same<T, TFrStatData>::value)
             return NULL;
        if (std::is_same<T, TFrSummary >::value)
             return ((FrSummary *) ptr)->next;
        if (std::is_same<T, TFrTable   >::value)
             return ((FrTable   *) ptr)->next;
        if (std::is_same<T, TFrVect    >::value)
             return ((FrVect    *) ptr)->next;

        return NULL;
    }

    template <typename T>
    TString TFrameProxy<T>::GetUniqueID(int basket)
    {
        return this->frame->GetUniqueID() 
            + (identifier.EqualTo("") ? "" : ("-" + identifier))
            + (basket < 0 ? "" : "-"+TString::Itoa(basket,10));
    }

    template <typename T>
    void TFrameProxy<T>::Dump(int basket)
    {
        this->Dump(this->GetName(basket));
    }

    template <typename T>
    void TFrameProxy<T>::Print(int basket)
    {
        this->Print(this->GetName(basket));
    }
                     
    template<typename T>
    void TFrameProxy<T>::Print(TString objName)
    {
        if (objName.EqualTo("")) { 

            for(int basket = 0, B = this->GetNBaskets(); basket < B; basket++)
                this->Print(basket);

            return;
        }

        TString objType = "";
        if (std::is_same<T, TFrAdcData>::value)
            objType = "TFrAdcData";
        if (std::is_same<T, TFrDetector>::value)
            objType = "TFrDetector";
        if (std::is_same<T, TFrEvent   >::value)
            objType = "TFrEvent";
        if (std::is_same<T, TFrHistory >::value)
            objType = "TFrHistory";
        if (std::is_same<T, TFrMsg     >::value)
            objType = "TFrMsg";
        if (std::is_same<T, TFrProcData>::value)
            objType = "TFrProcData";
        if (std::is_same<T, TFrRawData >::value)
            objType = "TFrRawData";
        if (std::is_same<T, TFrSerData >::value)
            objType = "TFrSerData";
        if (std::is_same<T, TFrSimData >::value)
            objType = "TFrSimData";
        if (std::is_same<T, TFrSimEvent>::value)
            objType = "TFrSimEvent";
        if (std::is_same<T, TFrStatData>::value)
            objType = "TFrStatData";
        if (std::is_same<T, TFrSummary >::value)
            objType = "TFrSummary";
        if (std::is_same<T, TFrTable   >::value)
            objType = "TFrTable";
        if (std::is_same<T, TFrVect    >::value)
            objType = "TFrVect";

        int maxClassLength = TString("FrProcData.Print").Length()+1;
        for(int i = 0; i < this->GetNBaskets(); i++) {

            if(!objName.EqualTo("") && !objName.EqualTo(this->GetName(i))) continue;
            if(objType.EqualTo("TFrRawData")) continue; // Raw Data is a data holding structure

            TString spacer = "";
            TString title = objType + ".Print ";
            TString channelInfo = "";

            if(this->Verbose(Verbosity::INFO)) {
    
                if(this->GetChannelNumber(i) >= 0) 
                    channelInfo = "Channel=" + TString::Itoa(this->GetChannelNumber(i),10);
                if(this->GetChannelGroup(i) >= 0) {

                    if(!channelInfo.EqualTo("")) channelInfo = " ";
                    channelInfo = "Group=" + TString::Itoa(this->GetChannelGroup(i),10);
                }
            
                if(!channelInfo.EqualTo("")) channelInfo = " ("+channelInfo+")";
        
            } else {
            
                if(this->GetChannelNumber(i) >= 0) 
                    channelInfo = "at channel " + TString::Itoa(this->GetChannelNumber(i),10);
                if(this->GetChannelGroup(i) >= 0) {
                    if(!channelInfo.EqualTo("")) channelInfo = " ";
                    channelInfo = "in group " + TString::Itoa(this->GetChannelGroup(i),10);
                }

                if(!channelInfo.EqualTo("")) channelInfo = " "+channelInfo;
            }

            TString name = "Name = \"" + this->GetName(i) + "\"";
            
            std::stringstream ss;

            spacer = ROOT::IOPlus::Helpers::Spacer(maxClassLength - title.Length());
            ss << TPrint::kPurple << title << spacer << " " << TPrint::kNoColor;
            
            spacer = ROOT::IOPlus::Helpers::Spacer(TPrint::CharCount(this->GetNBaskets()) - TPrint::CharCount(i+1));
            ss << "#" << (i+1) << "/" << this->GetNBaskets() << spacer;

            if(this->Verbose(Verbosity::DEBUG)) ss << " at \"" << this->GetPointer(i) << "\"" << channelInfo;
            
            std::cout << ss.str() << "; ";
            std::cout << name;
            
            TString msg = this->GetMessage(i);
            if(!msg.EqualTo("")) {
                
                spacer = ROOT::IOPlus::Helpers::Spacer(ss.str().size() - 9);
                if(this->Verbose(Verbosity::INFO)) std::cout << std::endl << spacer << "Comment = \"" << this->GetMessage(i) << "\"";
                else std::cout << "; Comment = \"[..]\"";
            }

            std::cout << std::endl;
        }
    }
     
    template<typename T>
    void TFrameProxy<T>::Dump(TString objName)
    {
        if (objName.EqualTo("")) { 

            for(int basket = 0, B = this->GetNBaskets(); basket < B; basket++)
                this->Dump(basket);

            return;
        }

        for(int i = 0; i < this->GetNBaskets(); i++) {

            if(!objName.EqualTo("") && !objName.EqualTo(this->GetName(i))) continue;

            void *ptr = this->GetPointer(i);
            int debugLvl = this->GetVerbosity();
            if (std::is_same<T, TFrAdcData>::value)
                FrAdcDataDump((FrAdcData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrDetector>::value)
                FrDetectorDump((FrDetector*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrEvent   >::value)
                FrEventDump((FrEvent*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrHistory >::value)
                fprintf(stdout," %s %s\n", FrStrGTime(((FrHistory*) ptr)->time), ((FrHistory*) ptr)->comment);
            if (std::is_same<T, TFrMsg     >::value)
                FrMsgDump((FrMsg *) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrProcData>::value)
                FrProcDataDump((FrProcData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrSerData >::value)
                FrSerDataDump((FrSerData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrSimData >::value)
                FrSimDataDump((FrSimData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrSimEvent>::value)
                FrSimEventDump((FrSimEvent*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrRawData>::value)
                FrRawDataDump((FrRawData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrStatData>::value)
                FrStatDataDump((FrStatData*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrSummary >::value)
                FrSummaryDump((FrSummary*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrTable   >::value)
                FrTableDump((FrTable*) ptr, stdout, debugLvl);
            if (std::is_same<T, TFrVect    >::value)
                FrVectDump((FrVect*) ptr, stdout, debugLvl);
        }
    }

    template<typename T>
    TString TFrameProxy<T>::GetType(bool demangle) {

        if(!demangle) {
            return typeid(T).name();
        }
        
        int status;
        char* demangled = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);

        if (status == 0) {

            TString result(demangled);
            free(demangled);
            return result;
        }
        
        return "";
    }

    template<typename T>
    TTree *TFrameProxy<T>::EmptyTree(TString fname, TString ftitle)
    {
        TTree *tree = new TTree(fname, ftitle);
               tree->SetAutoSave(0);

        // C : a character string terminated by the 0 character
        // B : an 1 bytes signed integer (Char_t)
        // b : an 1 bytes unsigned integer (UChar_t)
        // S : a 2 bytes signed integer (Short_t)
        // s : a 2 bytes unsigned integer (UShort_t)
        // I : a 4 bytes signed integer (Int_t)
        // i : a 4 bytes unsigned integer (UInt_t)
        // F : a 4 bytes floating point (Float_t)
        // f : a 3 bytes floating point with truncated mantissa (Float16_t)
        // D : a 8 bytes floating point (Double_t)
        // d : a 3 bytes truncated floating point (Double32_t)
        // L : a 8 bytes signed integer (Long64_t)
        // l : a 8 bytes unsigned integer (ULong64_t)
        // G : a long signed integer, stored as 8 bytes (Long_t)
        // g : a long unsigned integer, stored as 8 bytes (ULong_t)
        // O : [the letter o, not a zero] a boolean (Bool_t)

        tree->Branch("name",        &_buff_name);
        tree->Branch("run",         &_buff_run,         "run/I"        );
        tree->Branch("basket",      &_buff_basket,      "basket/I"     );
        tree->Branch("frame",       &_buff_frame,       "frame/i"      );
        tree->Branch("dataQuality", &_buff_dataQuality, "dataQuality/i");
        tree->Branch("GTimeS",      &_buff_GTimeS,      "GTimeS/i"     );
        tree->Branch("GTimeN",      &_buff_GTimeN,      "GTimeN/i"     );
        tree->Branch("ULeapS",      &_buff_ULeapS,      "ULeapS/s"     );
        tree->Branch("dt",          &_buff_dt,          "dt/D"         );

        TString objType = this->GetType();

        tree->Branch(TString(DEFAULT_BRANCHNAME)+".", objType, &_buff);
        return tree;
    }

    template<typename T>
    T* TFrameProxy<T>::Get(int basket)
    {
        T *obj = new T();
        this->Hydrate(this->GetPointer(basket), (T*) obj);
        return obj;
    }

    template<typename T>
    TObject* TFrameProxy<T>::GetObject(int basket)
    {
        TString name = this->GetName(basket);

        TTree *t = this->EmptyTree(name);
        if(t) this->Hydrate(basket, t);

        return t;
    }

    template<typename T>
    TObject* TFrameProxy<T>::GetObject(int basket, TString branchName, Option_t *option)
    {
        TTree *tree = (TTree *) this->GetObject(this->GetName(basket));
        TH1 *hData = TFrameProxyAbstract::Histogram(tree, branchName, option);

        tree->Delete();

        return (TObject *) hData;
    }

    template<typename T>
    int TFrameProxy<T>::Hydrate(int basket, TTree *t)
    {
        if(t == nullptr) return FR_ERROR_NO_FILE;
        if(basket < 0 || basket >= ptr.size()) return FR_ERROR_NO_FRAME;

        _buff_basket = basket;
        _buff_name = this->frame->ReadName().Data();
        _buff_run = this->frame->ReadRunNumber();
        _buff_frame = this->frame->ReadFrameNumber();
        _buff_dataQuality = this->frame->GetDataQuality();
        _buff_GTimeS = this->frame->ReadGTimeS();
        _buff_GTimeN = this->frame->ReadGTimeN();    
        _buff_ULeapS = this->frame->ReadULeapS();
        _buff_dt = this->frame->ReadLength();

        if (std::is_same<T, TFrAdcData>::value)
            this->Hydrate(ptr[basket], (TFrAdcData*) &_buff);
        if (std::is_same<T, TFrDetector>::value)
            this->Hydrate(ptr[basket], (TFrDetector*) &_buff);
        if (std::is_same<T, TFrEvent   >::value)
            this->Hydrate(ptr[basket], (TFrEvent*) &_buff);
        if (std::is_same<T, TFrHistory >::value)
            this->Hydrate(ptr[basket], (TFrHistory*) &_buff);
        if (std::is_same<T, TFrMsg>::value)
            this->Hydrate(ptr[basket], (TFrMsg*) &_buff);
        if (std::is_same<T, TFrProcData>::value)
            this->Hydrate(ptr[basket], (TFrProcData*) &_buff);
        if (std::is_same<T, TFrRawData>::value)
            this->Hydrate(ptr[basket], (TFrRawData*) &_buff);
        if (std::is_same<T, TFrSerData >::value)
            this->Hydrate(ptr[basket], (TFrSerData*) &_buff);
        if (std::is_same<T, TFrSimData >::value)
            this->Hydrate(ptr[basket], (TFrSimData*) &_buff);
        if (std::is_same<T, TFrSimEvent>::value)
            this->Hydrate(ptr[basket], (TFrSimEvent*) &_buff);
        if (std::is_same<T, TFrStatData>::value)
            this->Hydrate(ptr[basket], (TFrStatData*) &_buff);
        if (std::is_same<T, TFrSummary >::value)
            this->Hydrate(ptr[basket], (TFrSummary*) &_buff);
        if (std::is_same<T, TFrTable   >::value)
            this->Hydrate(ptr[basket], (TFrTable*) &_buff);
        if (std::is_same<T, TFrVect    >::value)
            this->Hydrate(ptr[basket], (TFrVect*) &_buff);

        t->Fill();
        return FR_OK;
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrAdcData *rootObject)
    {
        if(ptrFr   == nullptr) return;
        if(rootObject == nullptr) return;

        rootObject->name       = std::string(((FrAdcData*) ptrFr)->name ? ((FrAdcData*) ptrFr)->name : "");
        rootObject->comment    = std::string(((FrAdcData*) ptrFr)->comment ? ((FrAdcData*) ptrFr)->comment : "");
        rootObject->channelGroup  = ((FrAdcData*) ptrFr)->channelGroup;
        rootObject->channelNumber = ((FrAdcData*) ptrFr)->channelNumber;
        rootObject->nBits = ((FrAdcData*) ptrFr)->nBits;
        rootObject->bias  = ((FrAdcData*) ptrFr)->bias;
        rootObject->slope = ((FrAdcData*) ptrFr)->slope;
        rootObject->units = ((FrAdcData*) ptrFr)->units;
        rootObject->sampleRate = ((FrAdcData*) ptrFr)->sampleRate;
        rootObject->timeOffset = ((FrAdcData*) ptrFr)->timeOffset;
        rootObject->fShift     = ((FrAdcData*) ptrFr)->fShift;
        rootObject->phase      = ((FrAdcData*) ptrFr)->phase;
        rootObject->dataValid  = ((FrAdcData*) ptrFr)->dataValid;

        if(((FrAdcData*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrAdcData*) ptrFr)->data, &(rootObject->data));
        if(((FrAdcData*) ptrFr)->aux != NULL)
            this->Hydrate((void*) ((FrAdcData*) ptrFr)->aux , &(rootObject->aux));
    }
    
    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrDetector *rootObject)
    {
        rootObject->name         = std::string(((FrDetector*) ptrFr)->name ? ((FrDetector*) ptrFr)->name : "");
        rootObject->prefix[0]    = ((FrDetector*) ptrFr)->prefix[0];
        rootObject->prefix[1]    = ((FrDetector*) ptrFr)->prefix[1];
        rootObject->longitude    = ((FrDetector*) ptrFr)->longitude;
        rootObject->latitude     = ((FrDetector*) ptrFr)->latitude;
        rootObject->elevation    = ((FrDetector*) ptrFr)->elevation;
        rootObject->armXazimuth  = ((FrDetector*) ptrFr)->armXazimuth;
        rootObject->armYazimuth  = ((FrDetector*) ptrFr)->armYazimuth;
        rootObject->armXaltitude = ((FrDetector*) ptrFr)->armXaltitude;
        rootObject->armYaltitude = ((FrDetector*) ptrFr)->armYaltitude;
        rootObject->armXmidpoint = ((FrDetector*) ptrFr)->armXmidpoint;
        rootObject->armYmidpoint = ((FrDetector*) ptrFr)->armYmidpoint;

        rootObject->localTime = ((FrDetector*) ptrFr)->localTime;
        
        if(((FrDetector*) ptrFr)->aux != NULL)
            this->Hydrate((void*) ((FrDetector*) ptrFr)->aux  , &(rootObject->aux));
        if(((FrDetector*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrDetector*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrEvent *rootObject)
    {
        rootObject->name        = std::string(((FrEvent*) ptrFr)->name ? ((FrEvent*) ptrFr)->name : "");
        rootObject->comment     = std::string(((FrEvent*) ptrFr)->comment ? ((FrEvent*) ptrFr)->comment : "");
        rootObject->inputs      = std::string(((FrEvent*) ptrFr)->inputs ? ((FrEvent*) ptrFr)->inputs : "");
        rootObject->GTimeS      = ((FrEvent*) ptrFr)->GTimeS;
        rootObject->GTimeN      = ((FrEvent*) ptrFr)->GTimeN;
        rootObject->timeBefore  = ((FrEvent*) ptrFr)->timeBefore;
        rootObject->timeAfter   = ((FrEvent*) ptrFr)->timeAfter;
        rootObject->eventStatus = ((FrEvent*) ptrFr)->eventStatus;
        rootObject->amplitude   = ((FrEvent*) ptrFr)->amplitude;
        rootObject->probability = ((FrEvent*) ptrFr)->probability;
        rootObject->statistics  = ((FrEvent*) ptrFr)->statistics;

        rootObject->nParam         = ((FrEvent*) ptrFr)->nParam;
        rootObject->parameters     = std::vector<double> (((FrEvent*) ptrFr)->parameters    , ((FrEvent*) ptrFr)->parameters     + ((FrEvent*) ptrFr)->nParam);
        rootObject->parameterNames = std::vector<std::string> (((FrEvent*) ptrFr)->parameterNames, ((FrEvent*) ptrFr)->parameterNames + ((FrEvent*) ptrFr)->nParam);
        
        if(((FrEvent*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrEvent*) ptrFr)->data , &(rootObject->data));
        if(((FrEvent*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrEvent*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrHistory *rootObject)
    {
        rootObject->name    = std::string(((FrHistory*) ptrFr)->name);
        rootObject->time    = ((FrHistory*) ptrFr)->time;
        rootObject->comment = std::string(((FrHistory*) ptrFr)->comment);
    }
    
    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrMsg *rootObject)
    {
        rootObject->alarm    = std::string(((FrMsg*) ptrFr)->alarm ? ((FrMsg*) ptrFr)->alarm : "");
        rootObject->message  = std::string(((FrMsg*) ptrFr)->message ? ((FrMsg*) ptrFr)->message : "");
        rootObject->severity = ((FrMsg*) ptrFr)->severity;
        rootObject->GTimeS   = ((FrMsg*) ptrFr)->GTimeS;
        rootObject->GTimeN   = ((FrMsg*) ptrFr)->GTimeN;
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrProcData *rootObject)
    {
        rootObject->name         = std::string(((FrProcData*) ptrFr)->name ? ((FrProcData*) ptrFr)->name : "");
        rootObject->comment      = std::string(((FrProcData*) ptrFr)->comment ? ((FrProcData*) ptrFr)->comment : "");
        rootObject->type         = static_cast<TFrProcData::TYPE>   (((FrProcData*) ptrFr)->type);
        rootObject->subType      = static_cast<TFrProcData::SUBTYPE>(((FrProcData*) ptrFr)->subType);
        rootObject->timeOffset   = ((FrProcData*) ptrFr)->timeOffset;
        rootObject->tRange       = ((FrProcData*) ptrFr)->tRange;
        rootObject->fShift       = ((FrProcData*) ptrFr)->fShift;
        rootObject->phase        = ((FrProcData*) ptrFr)->phase;
        rootObject->fRange       = ((FrProcData*) ptrFr)->fRange;
        rootObject->BW           = ((FrProcData*) ptrFr)->BW;

        rootObject->nAuxParam     = ((FrProcData*) ptrFr)->nAuxParam;
        rootObject->auxParam      = std::vector<double>(((FrProcData*) ptrFr)->auxParam     , ((FrProcData*) ptrFr)->auxParam      + ((FrProcData*) ptrFr)->nAuxParam);
        rootObject->auxParamNames = std::vector<std::string>(((FrProcData*) ptrFr)->auxParamNames, ((FrProcData*) ptrFr)->auxParamNames + ((FrProcData*) ptrFr)->nAuxParam);

        if(((FrProcData*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrProcData*) ptrFr)->data , &(rootObject->data));
        if(((FrProcData*) ptrFr)->aux != NULL)
            this->Hydrate((void*) ((FrProcData*) ptrFr)->aux  , &(rootObject->aux));
        if(((FrProcData*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrProcData*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrRawData *rootObject)
    {
        rootObject->name = std::string(((FrRawData*) ptrFr)->name ? ((FrRawData*) ptrFr)->name : "");
        
        // Data holding structure
        // if(((FrRawData*) ptrFr)->firstSer != NULL)
        //     this->Hydrate((void*) ((FrRawData*) ptrFr)->firstSer, &(rootObject->firstSer));
        // if(((FrRawData*) ptrFr)->firstAdc != NULL)
        //     this->Hydrate((void*) ((FrRawData*) ptrFr)->firstAdc, &(rootObject->firstAdc));
        // if(((FrRawData*) ptrFr)->firstTable != NULL)
        //     this->Hydrate((void*) ((FrRawData*) ptrFr)->firstTable, &(rootObject->more));

        if(((FrRawData*) ptrFr)->logMsg != NULL)
            this->Hydrate((void*) ((FrRawData*) ptrFr)->logMsg, &(rootObject->logMsg));

        if(((FrRawData*) ptrFr)->more != NULL)
            this->Hydrate((void*) ((FrRawData*) ptrFr)->more, &(rootObject->more));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrSerData *rootObject)
    {
        rootObject->name       = std::string(((FrSerData*) ptrFr)->name ? ((FrSerData*) ptrFr)->name : "");
        rootObject->timeSec    = ((FrSerData*) ptrFr)->timeSec;
        rootObject->timeNsec   = ((FrSerData*) ptrFr)->timeNsec;
        rootObject->sampleRate = ((FrSerData*) ptrFr)->sampleRate;
        rootObject->data       = ((FrSerData*) ptrFr)->data;

        if(((FrSerData*) ptrFr)->serial != NULL)
            this->Hydrate((void*) ((FrSerData*) ptrFr)->serial, &(rootObject->serial));
        if(((FrSerData*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrSerData*) ptrFr)->table , &(rootObject->table ));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrSimData *rootObject)
    {
        rootObject->name       = std::string(((FrSimData*) ptrFr)->name ? ((FrSimData*) ptrFr)->name : "");
        rootObject->comment    = std::string(((FrSimData*) ptrFr)->comment ? ((FrSimData*) ptrFr)->comment : "");
        rootObject->sampleRate = ((FrSimData*) ptrFr)->sampleRate;
        rootObject->timeOffset = ((FrSimData*) ptrFr)->timeOffset;
        rootObject->fShift     = ((FrSimData*) ptrFr)->fShift;
        rootObject->phase      = ((FrSimData*) ptrFr)->phase;

        if(((FrSimData*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrSimData*) ptrFr)->data , &(rootObject->data ));
        if(((FrSimData*) ptrFr)->input != NULL)
            this->Hydrate((void*) ((FrSimData*) ptrFr)->input, &(rootObject->input));
        if(((FrSimData*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrSimData*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrSimEvent *rootObject)
    {
        rootObject->name    = std::string(((FrSimEvent*) ptrFr)->name ? ((FrSimEvent*) ptrFr)->name : "");
        rootObject->comment = std::string(((FrSimEvent*) ptrFr)->comment ? ((FrSimEvent*) ptrFr)->comment : "");
        rootObject->inputs  = std::string(((FrSimEvent*) ptrFr)->inputs ? ((FrSimEvent*) ptrFr)->inputs : "");
        rootObject->GTimeS  = ((FrSimEvent*) ptrFr)->GTimeS;
        rootObject->GTimeN  = ((FrSimEvent*) ptrFr)->GTimeN;

        rootObject->timeBefore = ((FrSimEvent*) ptrFr)->timeBefore;
        rootObject->timeAfter  = ((FrSimEvent*) ptrFr)->timeAfter;
        rootObject->amplitude  = ((FrSimEvent*) ptrFr)->amplitude;

        rootObject->nParam         = ((FrSimEvent*) ptrFr)->nParam;
        rootObject->parameters     = std::vector<double> (((FrSimEvent*) ptrFr)->parameters    , ((FrSimEvent*) ptrFr)->parameters     + ((FrSimEvent*) ptrFr)->nParam);
        rootObject->parameterNames = std::vector<std::string>  (((FrSimEvent*) ptrFr)->parameterNames, ((FrSimEvent*) ptrFr)->parameterNames + ((FrSimEvent*) ptrFr)->nParam);
        
        if(((FrSimEvent*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrSimEvent*) ptrFr)->data , &(rootObject->data));
        if(((FrSimEvent*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrSimEvent*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrStatData *rootObject)
    {
        rootObject->name           = std::string(((FrStatData*) ptrFr)->name ? ((FrStatData*) ptrFr)->name : "");
        rootObject->comment        = std::string(((FrStatData*) ptrFr)->comment ? ((FrStatData*) ptrFr)->comment : "");
        rootObject->representation = std::string(((FrStatData*) ptrFr)->representation ? ((FrStatData*) ptrFr)->representation : "");
        rootObject->timeStart      = ((FrStatData*) ptrFr)->timeStart;
        rootObject->timeEnd        = ((FrStatData*) ptrFr)->timeEnd;
        rootObject->version        = ((FrStatData*) ptrFr)->version;

        if(((FrStatData*) ptrFr)->detector != NULL)
            this->Hydrate((void*) ((FrStatData*) ptrFr)->detector, &(rootObject->detector));
        if(((FrStatData*) ptrFr)->data != NULL)
            this->Hydrate((void*) ((FrStatData*) ptrFr)->data , &(rootObject->data ));
        if(((FrStatData*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrStatData*) ptrFr)->table, &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrSummary *rootObject)
    {
        rootObject->name    = std::string(((FrSummary*) ptrFr)->name ? ((FrSummary*) ptrFr)->name : "");
        rootObject->comment = std::string(((FrSummary*) ptrFr)->comment ? ((FrSummary*) ptrFr)->comment : "");
        rootObject->test    = std::string(((FrSummary*) ptrFr)->test ? ((FrSummary*) ptrFr)->test : "");
        rootObject->GTimeS  = ((FrSummary*) ptrFr)->GTimeS;
        rootObject->GTimeN  = ((FrSummary*) ptrFr)->GTimeN;

        if(((FrSummary*) ptrFr)->moments != NULL)
            this->Hydrate((void*) ((FrSummary*) ptrFr)->moments, &(rootObject->moments));
        if(((FrSummary*) ptrFr)->table != NULL)
            this->Hydrate((void*) ((FrSummary*) ptrFr)->table  , &(rootObject->table));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrTable *rootObject)
    {
        rootObject->name       = std::string(((FrTable*) ptrFr)->name ? ((FrTable*) ptrFr)->name : "");
        rootObject->comment    = std::string(((FrTable*) ptrFr)->comment ? ((FrTable*) ptrFr)->comment : "");

        rootObject->nColumn    = ((FrTable*) ptrFr)->nColumn;
        rootObject->nRow       = ((FrTable*) ptrFr)->nRow;
        rootObject->columnName = std::vector<std::string> (((FrTable*) ptrFr)->columnName , ((FrTable*) ptrFr)->columnName  + ((FrTable*) ptrFr)->nColumn);

        this->Hydrate((void*) ((FrTable*) ptrFr)->column, &(rootObject->column));
    }

    template<typename T>
    void TFrameProxy<T>::Hydrate(void *ptrFr, TFrVect *rootObject)
    {
        rootObject->name     = std::string(((FrVect*) ptrFr)->name ? ((FrVect*) ptrFr)->name : "");
        rootObject->compress = ((FrVect*) ptrFr)->compress;
        rootObject->type     = static_cast<TFrVect::TYPE> (((FrVect*) ptrFr)->type);

        rootObject->nDim     = ((FrVect*) ptrFr)->nDim;
        rootObject->nBytes   = ((FrVect*) ptrFr)->nBytes;
        rootObject->nData    = ((FrVect*) ptrFr)->nData;

        if( ((FrVect*) ptrFr)->nData > 0 ) {

            if ( ((FrVect*) ptrFr)->nDim > 0) {
                rootObject->nx      = std::vector<uint>(((FrVect*) ptrFr)->nx    , ((FrVect*) ptrFr)->nx     + ((FrVect*) ptrFr)->nDim);
                rootObject->dx      = std::vector<double>(((FrVect*) ptrFr)->dx    , ((FrVect*) ptrFr)->dx     + ((FrVect*) ptrFr)->nDim);
                rootObject->startX  = std::vector<double>(((FrVect*) ptrFr)->startX, ((FrVect*) ptrFr)->startX + ((FrVect*) ptrFr)->nDim);
                rootObject->unitX   = std::vector<std::string> (((FrVect*) ptrFr)->unitX , ((FrVect*) ptrFr)->unitX  + ((FrVect*) ptrFr)->nDim);
            }

            if (( (FrVect*) ptrFr)->nBytes > 0) {

                rootObject->data  = FrCast<double>((FrVect*) ptrFr);

                unsigned long effectiveSize;
                switch(rootObject->type) {

                    case TFrVect::TYPE::FR_VECT_STRING : effectiveSize = -1;
                        break;

                    case TFrVect::TYPE::FR_VECT_8C  : [[fallthrough]];
                    case TFrVect::TYPE::FR_VECT_16C : effectiveSize = rootObject->nData/2;
                        break;
                    
                    default: effectiveSize = rootObject->nData;
                }

                if( effectiveSize > 0 && effectiveSize != rootObject->nData )
                    TPrint::Error(__METHOD_NAME__, Form("Wrong vector size (%lu) received.. %lu expected", effectiveSize, rootObject->nData));
            }

            rootObject->unitY    = std::string(((FrVect*) ptrFr)->unitY ? ((FrVect*) ptrFr)->unitY : "");
        }
    }

    template <typename T>
    TString TFrameProxy<T>::ReadName(void *ptr)
    {
        if (std::is_same<T, TFrAdcData>::value)
            return ((FrAdcData  *) ptr)->name;
        if (std::is_same<T, TFrDetector>::value)
             return ((FrDetector*) ptr)->name;
        if (std::is_same<T, TFrEvent   >::value)
             return ((FrEvent   *) ptr)->name;
        if (std::is_same<T, TFrHistory >::value)
             return ((FrHistory *) ptr)->name;
        if (std::is_same<T, TFrMsg>::value)
             return ((FrMsg     *) ptr)->alarm;
        if (std::is_same<T, TFrProcData>::value)
             return ((FrProcData*) ptr)->name;
        if (std::is_same<T, TFrRawData >::value)
             return ((FrRawData *) ptr)->name;
        if (std::is_same<T, TFrSerData >::value)
             return ((FrSerData *) ptr)->name;
        if (std::is_same<T, TFrSimData >::value)
             return ((FrSimData *) ptr)->name;
        if (std::is_same<T, TFrSimEvent>::value)
             return ((FrSimEvent*) ptr)->name;
        if (std::is_same<T, TFrStatData>::value)
             return ((FrStatData*) ptr)->name;
        if (std::is_same<T, TFrSummary >::value)
             return ((FrSummary *) ptr)->name;
        if (std::is_same<T, TFrTable   >::value)
             return ((FrTable   *) ptr)->name;
        if (std::is_same<T, TFrVect    >::value)
             return ((FrVect    *) ptr)->name;

        return "";
    }

    template <typename T>
    double TFrameProxy<T>::ReadSampleRate(void *ptr)
    {
        if (std::is_same<T, TFrAdcData>::value)
            return ((FrAdcData  *) ptr)->sampleRate;
        if (std::is_same<T, TFrDetector>::value)
             return 0;
        if (std::is_same<T, TFrEvent   >::value)
             return 0;
        if (std::is_same<T, TFrHistory >::value)
             return 0;
        if (std::is_same<T, TFrMsg>::value)
             return 0;
        if (std::is_same<T, TFrProcData>::value) {
            double dx = *(((FrProcData *) ptr)->data->dx);
            return dx ? 1./dx : 0;
        }
        
        if (std::is_same<T, TFrRawData >::value)
             return 0;
        if (std::is_same<T, TFrSerData >::value)
             return ((FrSerData *) ptr)->sampleRate;
        if (std::is_same<T, TFrSimData >::value)
             return ((FrSimData *) ptr)->sampleRate;
        if (std::is_same<T, TFrSimEvent>::value)
             return 0;
        if (std::is_same<T, TFrStatData>::value)
             return 0;
        if (std::is_same<T, TFrSummary >::value)
             return 0;
        if (std::is_same<T, TFrTable   >::value)
             return 0;
        if (std::is_same<T, TFrVect    >::value) {
            double dx = *(((FrVect    *) ptr)->dx);
            return dx ? 1./dx : 0;
        }

        return 0;
    }

    template <typename T>
    TString TFrameProxy<T>::ReadMessage(void *ptr)
    {
        if (std::is_same<T, TFrAdcData>::value)
            return ((FrAdcData  *) ptr)->comment;
        if (std::is_same<T, TFrDetector>::value)
            return ((FrDetector*) ptr)->prefix[0] + ((FrDetector*) ptr)->prefix[1];
        if (std::is_same<T, TFrEvent   >::value)
            return ((FrEvent   *) ptr)->comment;
        if (std::is_same<T, TFrHistory >::value)
            return ((FrHistory *) ptr)->comment;
        if (std::is_same<T, TFrMsg>::value)
            return ((FrMsg     *) ptr)->message;
        if (std::is_same<T, TFrProcData>::value)
            return ((FrProcData*) ptr)->comment;
        if (std::is_same<T, TFrRawData >::value)
            return "";
        if (std::is_same<T, TFrSerData >::value)
            return ((FrSerData *) ptr)->data;
        if (std::is_same<T, TFrSimData >::value)
            return ((FrSimData *) ptr)->comment;
        if (std::is_same<T, TFrSimEvent>::value)
            return ((FrSimEvent*) ptr)->comment;
        if (std::is_same<T, TFrStatData>::value)
            return ((FrSimEvent*) ptr)->comment;
        if (std::is_same<T, TFrSummary >::value)
            return ((FrSummary *) ptr)->comment;
        if (std::is_same<T, TFrTable   >::value)
            return ((FrTable   *) ptr)->comment;
        if (std::is_same<T, TFrVect    >::value)
            return ((FrVect    *) ptr)->unitY;

        return "";
    }

    template <typename T>
    TString TFrameProxy<T>::GetBranchName()
    {
        if (std::is_same<T, TFrAdcData>::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrDetector>::value)
            return TString(DEFAULT_BRANCHNAME)+".aux";
        if (std::is_same<T, TFrEvent   >::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrHistory >::value)
            return "";
        if (std::is_same<T, TFrMsg>::value)
            return "";
        if (std::is_same<T, TFrProcData>::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrRawData >::value)
            return TString(DEFAULT_BRANCHNAME)+".more";
        if (std::is_same<T, TFrSerData >::value)
            return TString(DEFAULT_BRANCHNAME)+".serial";
        if (std::is_same<T, TFrSimData >::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrSimEvent>::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrStatData>::value)
            return TString(DEFAULT_BRANCHNAME)+".data";
        if (std::is_same<T, TFrSummary >::value)
            return TString(DEFAULT_BRANCHNAME)+".moments";
        if (std::is_same<T, TFrTable   >::value)
            return TString(DEFAULT_BRANCHNAME)+".column";
        if (std::is_same<T, TFrVect    >::value)
            return "";

        return "";
    }

    template <typename T>
    int TFrameProxy<T>::ReadChannelNumber(void *ptr)
    {
        if (std::is_same<T, TFrAdcData>::value) return -1;
        
        return ((FrAdcData *) ptr)->channelNumber;
    }

    template <typename T>
    int TFrameProxy<T>::ReadChannelGroup(void *ptr)
    {
        if (std::is_same<T, TFrAdcData>::value) return -1;
        
        return ((FrAdcData *) ptr)->channelGroup;
    }

    template class TFrameProxy<TFrAdcData>;
    template class TFrameProxy<TFrDetector>;
    template class TFrameProxy<TFrEvent>;
    template class TFrameProxy<TFrHistory>;
    template class TFrameProxy<TFrMsg>;
    template class TFrameProxy<TFrProcData>;
    template class TFrameProxy<TFrRawData>;
    template class TFrameProxy<TFrSerData>;
    template class TFrameProxy<TFrSimData>;
    template class TFrameProxy<TFrSimEvent>;
    template class TFrameProxy<TFrStatData>;
    template class TFrameProxy<TFrSummary>;
    template class TFrameProxy<TFrTable>;
    template class TFrameProxy<TFrVect>;

}
