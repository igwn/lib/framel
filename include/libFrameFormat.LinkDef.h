#ifdef __CINT__

#pragma link off all globals;
#pragma link off all functions;

#pragma link C++ class FrameFormat::TFrameAbstract+;
#pragma link C++ class FrameFormat::TFrameChain+;
#pragma link C++ class FrameFormat::TFrameManager+;

#pragma link C++ class FrameFormat::TFrame+;
#pragma link C++ enum class FrameFormat::FrType+;

#pragma link C++ struct FrameFormat::TFrAbstract+;

#pragma link C++ struct FrameFormat::TFrVect+;
#pragma link C++ struct FrameFormat::TFrTable+;
#pragma link C++ struct FrameFormat::TFrDetector+;
#pragma link C++ struct FrameFormat::TFrAdcData+;
#pragma link C++ struct FrameFormat::TFrEvent+;
#pragma link C++ struct FrameFormat::TFrSimEvent+;
#pragma link C++ struct FrameFormat::TFrHistory+;
#pragma link C++ struct FrameFormat::TFrMsg+;
#pragma link C++ struct FrameFormat::TFrProcData+;
#pragma link C++ enum class FrameFormat::TFrProcData::type+;
#pragma link C++ enum class FrameFormat::TFrProcData::subtype+;
#pragma link C++ struct FrameFormat::TFrSerData+;
#pragma link C++ struct FrameFormat::TFrRawData+;
#pragma link C++ struct FrameFormat::TFrSimData+;
#pragma link C++ struct FrameFormat::TFrStatData+;
#pragma link C++ struct FrameFormat::TFrSummary+;


#pragma link C++ class FrameFormat::TFrameProxyAbstract;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrAdcData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrDetector>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrEvent>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrHistory>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrMsg>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrProcData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrRawData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrSerData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrSimData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrSimEvent>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrStatData>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrSummary>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrTable>;
#pragma link C++ class FrameFormat::TFrameProxy<FrameFormat::TFrVect>;


#endif
