#pragma once

#include "FrameFormat/Abstract/TFrameAbstract.h"

#include "FrameFormat/TFrameChain.h"
#include "FrameFormat/TFrameManager.h"

namespace FrameFormat {

        template <typename T>
        class TFrameProxy;
        class TFrameProxyAbstract;

        using TFrameProxyT = std::variant<
                FrameFormat::TFrameProxy<FrameFormat::TFrAdcData>*, 
                FrameFormat::TFrameProxy<FrameFormat::TFrDetector>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrEvent>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrHistory>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrMsg>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrProcData>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrRawData>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrSerData>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrSimData>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrSimEvent>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrStatData>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrSummary>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrTable>*,
                FrameFormat::TFrameProxy<FrameFormat::TFrVect>*
        >;

        class TFrame: public TFrameAbstract
        {
                private:
                        friend class TFrameChain;
                        friend class TFrameManager;

                        using TObject::Print;
                        using TObject::Read;
                        using TObject::Write;
                        using TObject::Dump;
                        using TObject::GetUniqueID;

                protected:

                        TFrameChain *fChain;
                        inline TFrameManager &GetManager() { return this->fChain->GetManager(); }

                        int fileID;
                        inline int GetFileID() { return this->fileID; }
                        int frameID;
                        inline int GetFrameID() { return this->frameID; }

                        GPS gps;
                        unsigned int dataQuality;

                        std::vector<TFrame *> fSiblings;

                        TFrameRHead &GetRHead();
                        
                        std::vector<FrType> dataClass;
                        std::vector<TFrameProxyT> proxyMap;

                        TString tags;
                        std::vector<TPRegexp> tagRegex;
                        std::vector<TPRegexp> tagAntiRegex;

                        int decimate;
                        std::vector<int> channelGroups;
                        std::vector<int> channelNumbers;

                        // Methods returning proxies
                        std::vector<TFrameProxyT> ReadProxy();

                        template <typename T>
                        TFrameProxyT ReadProxy(void *);
                        template <typename T>
                        inline std::vector<TFrameProxyT> ReadProxy(TString identifier) { return this->ReadProxy<T>(std::vector<TString>({identifier})); };
                        template <typename T>
                        std::vector<TFrameProxyT> ReadProxy(std::vector<TString> identifierList);
                        
                        inline int GetNProxies() { return this->proxyMap.size(); }
                        std::vector<FrType> GetProxyDataType();

                        // Generic methods returning a std::variant
                        inline std::vector<TFrAbstractT> Read(const std::type_index typeIdx) { return this->Read(TString("*"), typeIdx); }
                        inline std::vector<TFrAbstractT> Read(TString identifier, const std::type_index typeIdx = typeid(void)) { return this->Read(std::vector<TString>({identifier}), typeIdx); };
                               std::vector<TFrAbstractT> Read(std::vector<TString> identifierList, const std::type_index typeIdx = typeid(void));

                public:
                
                        TFrame(TFrameChain *fChain, int fileID, int frameID, TString tags = "*", std::vector<FrType> dataClass = {}, int decimate = 1, std::vector<int> channelGroups = {}, std::vector<int> channelNumbers = {});
                        ~TFrame();

                        void Dump();
                        bool Load();
                        void Free();
                        bool Ready();

                        TFrame *Prev();
                        TFrame *Next();
        
                        bool IsValidTag(TString tag);
                        bool IsValidDataClass(FrType);

                        std::vector<TString> GetDataTags();
                        inline std::vector<FrType> GetDataClass() { return this->dataClass; }

                        inline int GetDecimate() { return decimate; }
                        inline std::vector<int> GetChannelGroups() { return channelGroups; }
                        inline std::vector<int> GetChannelNumbers() { return channelNumbers; };

                        TString GetFileName();
                        TString GetUniqueID();

                        GPS GetGPS();
                        TFrame &SetGPS(GPS gps);
                        double GetDuration();

                        bool AddFriend(TFrame *frame);
                        void RemoveFriend(TFrame *frame);

                        unsigned int GetDataQuality();
                        TFrame &SetDataQuality(unsigned int);

                        inline int GetN() { return GetAdcNames().size() + GetProcNames().size() + GetSimNames().size() + GetSerNames().size() + GetSummaryNames().size(); }
                        std::vector<TString> GetAdcNames();
                        std::vector<TString> GetProcNames();
                        std::vector<TString> GetSimNames();
                        std::vector<TString> GetSerNames();
                        std::vector<TString> GetSummaryNames();

                        bool IsContiguous();
                        
                        TString ReadName();
                        int ReadRunNumber();
                        int ReadFrameNumber();

                        unsigned int ReadGTimeS();
                        unsigned int ReadGTimeN();
                        unsigned short ReadULeapS();
                        double ReadLength();

                        bool ReadSimulationFlag();

                        // Standard reading methods
                        TFrame &Read();

                        // Reading methods returning TFr structures
                        template <typename T>
                        inline std::vector<T*> Read() { return this->Read<T>(TString("*")); }
                        template <typename T>
                        inline std::vector<T*> Read(TString identifier) { return this->Read<T>(std::vector<TString>({identifier})); };
                        template <typename T>
                        inline std::vector<T*> Read(std::vector<TString> tagList)
                        {
                                std::vector<TFrAbstractT> _objList = Read(tagList, typeid(T));

                                std::vector<T*> objList;
                                for(int i = 0, N = _objList.size(); i < N; i++) {

                                        objList.push_back(std::get<T*>(_objList[i]));
                                }

                                return objList;
                        }

                        // Return the TFr classes in use (to be combined with public Read<T*> methods)
                        std::vector<FrType> ReadTypes();

                        inline std::vector<TFrRawData  *> ReadRawData (TString identifier) { return this->Read<TFrRawData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrAdcData  *> ReadAdcData (TString identifier) { return this->Read<TFrAdcData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrProcData *> ReadProcData(TString identifier) { return this->Read<TFrProcData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrSimData  *> ReadSimData (TString identifier) { return this->Read<TFrSimData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrSerData  *> ReadSerData (TString identifier) { return this->Read<TFrSerData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrSummary  *> ReadSummary (TString identifier) { return this->Read<TFrSummary>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrEvent    *> ReadEvent   (TString identifier) { return this->Read<TFrEvent>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrHistory  *> ReadHistory (TString identifier) { return this->Read<TFrHistory>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrStatData *> ReadStatData(TString identifier) { return this->Read<TFrStatData>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrDetector *> ReadDetector(TString identifier) { return this->Read<TFrDetector>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrTable    *> ReadTable   (TString identifier) { return this->Read<TFrTable>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrMsg      *> ReadMsg     (TString identifier) { return this->Read<TFrMsg>(std::vector<TString>({identifier})); };
                        inline std::vector<TFrVect     *> ReadVect    (TString identifier) { return this->Read<TFrVect>(std::vector<TString>({identifier})); };

                        inline std::vector<TFrRawData  *> ReadRawData (std::vector<TString> identifierList = {}) { return this->Read<TFrRawData>(identifierList); }
                        inline std::vector<TFrAdcData  *> ReadAdcData (std::vector<TString> identifierList = {}) { return this->Read<TFrAdcData>(identifierList); }
                        inline std::vector<TFrProcData *> ReadProcData(std::vector<TString> identifierList = {}) { return this->Read<TFrProcData>(identifierList); }
                        inline std::vector<TFrSimData  *> ReadSimData (std::vector<TString> identifierList = {}) { return this->Read<TFrSimData>(identifierList); }
                        inline std::vector<TFrSerData  *> ReadSerData (std::vector<TString> identifierList = {}) { return this->Read<TFrSerData>(identifierList); }
                        inline std::vector<TFrSummary  *> ReadSummary (std::vector<TString> identifierList = {}) { return this->Read<TFrSummary>(identifierList); }
                        inline std::vector<TFrEvent    *> ReadEvent   (std::vector<TString> identifierList = {}) { return this->Read<TFrEvent>(identifierList); }
                        inline std::vector<TFrHistory  *> ReadHistory (std::vector<TString> identifierList = {}) { return this->Read<TFrHistory>(identifierList); }
                        inline std::vector<TFrStatData *> ReadStatData(std::vector<TString> identifierList = {}) { return this->Read<TFrStatData>(identifierList); }
                        inline std::vector<TFrDetector *> ReadDetector(std::vector<TString> identifierList = {}) { return this->Read<TFrDetector>(identifierList); }
                        inline std::vector<TFrTable    *> ReadTable   (std::vector<TString> identifierList = {}) { return this->Read<TFrTable>(identifierList); }
                        inline std::vector<TFrMsg      *> ReadMsg     (std::vector<TString> identifierList = {}) { return this->Read<TFrMsg>(identifierList); }
                        inline std::vector<TFrVect     *> ReadVect    (std::vector<TString> identifierList = {}) { return this->Read<TFrVect>(identifierList); }

                        void Print(Option_t *opt = "");

                ClassDef(TFrame,1);
        };
};
