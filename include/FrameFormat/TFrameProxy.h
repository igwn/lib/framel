#pragma once

#include "FrameFormat/Abstract/TFrameAbstract.h"

namespace FrameFormat {

       class TFrame;

       class TFrameProxyAbstract : public TFrameAbstract{

              friend class TFrame;
              friend class TFrameChain;

              protected:
                     static TH1 *Histogram(TTree *tree, TString branchName, Option_t *option = "");

              public:

                     static const char *DEFAULT_BRANCHNAME;

                     virtual ~TFrameProxyAbstract() = default;
                     virtual TString GetIdentifier() = 0;

                     virtual TTree *EmptyTree(TString name, TString title = "") = 0;
                     virtual int Hydrate(TString objName, TTree *tree) = 0;
                     virtual int Hydrate(int basket, TTree *tree) = 0;

                     virtual TString GetName(int basket) = 0;
                     virtual TString GetMessage(int basket) = 0;
                     virtual TString GetBranchName() = 0;

                     virtual TObject *GetObject(TString objName, TString branchName, Option_t *opt = "") = 0;
                     virtual TObject *GetObject(TString objName) = 0;

                     virtual TObject *GetObject(int basket, TString branchName, Option_t *opt = "") = 0;
                     virtual TObject *GetObject(int basket) = 0;
                            
                     virtual int GetBasketID(void *) = 0;
                     virtual int GetNBaskets() = 0;
       };

       template <typename T>
       class TFrameProxy : public TFrameProxyAbstract
       {
              static_assert(std::is_base_of<TFrAbstract, T>::value, "Template must declare a class based on TFrAbstract");

              private:

                     using TObject::Print;
                     using TObject::Write;
                     using TObject::GetUniqueID;
                     using TObject::GetName;
                     using TObject::Dump;

                     T _buff;

                     int          _buff_run;
                     int          _buff_basket;
                     std::string  _buff_name;
                     unsigned int _buff_frame;
                     unsigned int _buff_dataQuality;
                     unsigned int _buff_GTimeS;
                     unsigned int _buff_GTimeN;
                     ushort       _buff_ULeapS;
                     double       _buff_dt;
                     double       _buff_GPS;
                     TFrVect      _buff_vect;


                     TString ReadName(void *ptr);
                     TString ReadMessage(void *ptr);

                     int ReadChannelGroup(void *ptr);
                     int ReadChannelNumber(void *ptr);
                     double ReadSampleRate(void *ptr);

              protected:

                     TString tag;

                     std::vector<void *> ptr;
                     std::vector<TString> names;
                     std::vector<TString> messages;
                     std::vector<int> channelGroups;
                     std::vector<int> channelNumbers;
                     std::vector<double> sampleRates;

                     TString identifier;
                     TFrame *frame;

                     void* Next(void *ptr);
                     void* GetPointer(int basket);
                     int GetBasketID(void *ptr);

                     TTree *EmptyTree(TString name, TString title = "");
                     inline int Hydrate(TString objName, TTree *tree) { return Hydrate(this->GetBasketID(objName), tree); }
                            int Hydrate(int basket, TTree *tree);

              public:

                     TFrameProxy(TFrame *frame, TString _identifier, void *_ptr);
                     ~TFrameProxy() {};
                     
                     TString GetUniqueID(int basket);
                     inline TString GetIdentifier() { return this->identifier; }
                     inline int GetNBaskets() { return this->ptr.size(); }

                     int GetBasketID(TString objName);
                     TString GetName(int basket);

                     inline T *Get(TString objName) { return Get(this->GetBasketID(objName)); }
                            T *Get(int basket);
              
                     inline TObject *GetObject(TString objName) { return GetObject(this->GetBasketID(objName)); }
                            TObject *GetObject(int basket);
                            
                     inline TObject *GetObject(TString objName, TString branchName, Option_t *opt = "") { return GetObject(this->GetBasketID(objName), branchName, opt); }
                            TObject *GetObject(int basket, TString branchName, Option_t *opt = "");
                            
                     TString GetType(bool demangle = true);
                     TString GetBranchName();

                     inline TString GetMessage(TString objName) { return GetMessage(this->GetBasketID(objName)); }
                            TString GetMessage(int basket);

                     inline int GetChannelGroup(TString objName) { return GetChannelGroup(this->GetBasketID(objName)); }
                            int GetChannelGroup(int basket);
                     inline int GetChannelNumber(TString objName) { return GetChannelNumber(this->GetBasketID(objName)); }
                            int GetChannelNumber(int basket);
                     inline double GetSampleRate(TString objName) { return GetSampleRate(this->GetBasketID(objName)); }
                            double GetSampleRate(int basket);

                     void Hydrate(void *ptr, TFrAdcData *frObject);
                     void Hydrate(void *ptr, TFrDetector *frObject);
                     void Hydrate(void *ptr, TFrEvent *frObject);
                     void Hydrate(void *ptr, TFrHistory *frObject);
                     void Hydrate(void *ptr, TFrMsg *frObject);
                     void Hydrate(void *ptr, TFrProcData *frObject);
                     void Hydrate(void *ptr, TFrRawData *frObject);
                     void Hydrate(void *ptr, TFrSerData *frObject);
                     void Hydrate(void *ptr, TFrSimData *frObject);
                     void Hydrate(void *ptr, TFrSimEvent *frObject);
                     void Hydrate(void *ptr, TFrStatData *frObject);
                     void Hydrate(void *ptr, TFrSummary *frObject);
                     void Hydrate(void *ptr, TFrTable *frObject);
                     void Hydrate(void *ptr, TFrVect *frObject);

                     void Print(TString objName = "");
                     void Print(int basket);

                     void Dump(TString objName = "");
                     void Dump(int basket);

              ClassDef(TFrameProxy,1);
       };
}
