#pragma once

#include "FrameFormat/Impl/TFrameHeader.h"
#include "FrameFormat/TFrameChain.h"

namespace FrameFormat {

        // Reading head 
        class TFrameRHead { 

                int id;

                FrFile *fPointer;

                public:
                        TFrameRHead(int id, FrFile *fPointer): id(id), fPointer(fPointer) { }
                        
                        inline int GetID() { return id; }
                        inline FrFile *GetFile() { return fPointer; }
        };

        class TFrameManager: public TFrameAbstract
        {
                public:
                        enum class EIndexing {kFile, kFrame};

                private:
                        
                        friend class TFrameChain;

                        using TObject::GetName;
                        using TObject::GetUniqueID;
                        using TObject::Dump;
                        using TObject::Write;
                        using TObject::Read;
                        using TObject::Print;

                protected:

                        TFrameManager(FrFile &fMaster, std::vector<int> frameAllocation);

                        FrFile &fMaster;
                        std::vector<TFrameRHead*> fHead;

                        int GetNFiles(FrFile *file);
                        int GetNFrames(FrFile *file, int fileID = -1);

                        bool Seek  (FrFile *f, int fileID = -1, int frameID = -1);
                        bool Rewind(FrFile *f, int fileID = -1);
                        int  Tell  (FrFile *f, EIndexing index);

                        bool PreviousFile(FrFile *f);
                        bool Previous(FrFile *f);
                        bool Next(FrFile *f);
                        bool NextFile(FrFile *f);

                        FrameH*      ReadHeader(FrFile *f, int fileID = -1);
                        FrTOC*       ReadTOC   (FrFile *f, int fileID = -1);
                        FrEndOfFile* ReadEOF   (FrFile *f, int fileID = -1);
                        FrameH*      Read      (FrFile *f, int fileID = -1, int frameID = -1, const char * tags = "*");
                        FrameH*      Read      (FrFile *f, int fileID,      int frameID,      std::vector<TString> tags);

                        void DumpTOC (FrFile *f, int fileID, std::vector<TString> tags);
                        void DumpTOC (FrFile *f, int fileID = -1, const char * tags = "*"  );
                        
                        std::map<FrFile*, std::vector<std::vector<FrameH*>>> frame;
                        bool Find(FrFile*, int fileID, int frameID);
                        void Free(FrFile*, int fileID = -1, int frameID = -1);

                        GPS GetGPS(FrFile *f);
                        GPS GetGPS(FrFile *f, int fileID, int frameID = -1);
                        double GetDuration(FrFile *f);
                        double GetDuration(FrFile *f, int fileID, int frameID = -1);

                        unsigned int GetDataQuality  (FrFile *f, int fileID = -1, int frameID = -1);

                public:

                        ~TFrameManager();

                        int GetFileID(FrFile *file, FrFileH *fileHeader = NULL);
                        int GetFrameID(FrFile *file, FrameH *frame = NULL);

                        std::vector<TString> GetFileNames(FrFile *file);

                        FrFile &GetMaster(int fileID = -1, int frameID = -1);
                        TFrameRHead *GetRHead(int head);

                        inline TString GetUniqueID(TFrameRHead &rHead, int fileID, int frameID);
                        inline TString GetUniqueID                     (int fileID, int frameID);

                        FrFile *GetFile(TFrameRHead &rHead, int fileID = -1, int frameID = -1);

                        inline GPS GetGPS(TFrameRHead *rHead, int fileID, int frameID = -1) { return GetGPS(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline GPS GetGPS(int fileID = -1, int frameID = -1) { return GetGPS(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }
                        inline double GetDuration(TFrameRHead *rHead, int fileID, int frameID = -1) { return GetDuration(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline double GetDuration(int fileID = -1, int frameID = -1) { return GetDuration(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }
                        inline unsigned int GetDataQuality(TFrameRHead *rHead, int fileID = -1, int frameID = -1) { return GetDataQuality(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline unsigned int GetDataQuality(int fileID = -1, int frameID = -1) { return GetDataQuality(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }

                        inline int GetNFiles(TFrameRHead *rHead) { return GetNFiles(rHead ? rHead->GetFile() : NULL); }
                        inline int GetNFiles()                   { return GetNFiles(this->GetRHead(0)); }
                        inline int GetNFrames(TFrameRHead *rHead, int fileID) { return GetNFrames(rHead ? rHead->GetFile() : NULL, fileID); }
                        inline int GetNFrames                    (int fileID) { return GetNFrames(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID); }

                        inline int  Tell  (TFrameRHead *rHead, EIndexing index) { return Tell(rHead ? rHead->GetFile() : NULL, index); }
                        inline int  Tell                      (EIndexing index) { return Tell(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, index); }
                        inline bool Seek  (TFrameRHead *rHead, int fileID, int frameID) { return Seek(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline bool Seek                      (int fileID, int frameID) { return Seek(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }
                        inline bool Rewind(TFrameRHead *rHead, int fileID = 0) { return Rewind(rHead ? rHead->GetFile() : NULL, fileID); }
                        inline bool Rewind                    (int fileID = 0) { return Rewind(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID); }

                        inline bool PreviousFile(TFrameRHead *rHead) { return PreviousFile(rHead); }
                        inline bool PreviousFile()                   { return PreviousFile(this->GetRHead(0)); }
                        inline bool Previous(TFrameRHead *rHead) { return Previous(rHead); }
                        inline bool Previous()                   { return Previous(this->GetRHead(0)); }
                        inline bool Next(TFrameRHead *rHead) { return Next(rHead); }
                        inline bool Next()                   { return Next(this->GetRHead(0)); }
                        inline bool NextFile(TFrameRHead *rHead) { return NextFile(rHead); }
                        inline bool NextFile()                   { return NextFile(this->GetRHead(0)); }

                        inline FrameH*      ReadHeader(TFrameRHead *rHead, int fileID = -1) { return ReadHeader(rHead ? rHead->GetFile() : NULL, fileID); }
                        inline FrameH*      ReadHeader                    (int fileID = -1) { return ReadHeader(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID); }
                        inline FrTOC*       ReadTOC  (TFrameRHead *rHead,  int fileID = -1) { return ReadTOC(rHead ? rHead->GetFile() : NULL, fileID); }
                        inline FrTOC*       ReadTOC                       (int fileID = -1) { return ReadTOC(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID); }
                        inline FrameH*      Read     (TFrameRHead *rHead,  int fileID = -1, int frameID = -1, const char * tags = "*"  ) { return Read(rHead ? rHead->GetFile() : NULL, fileID, frameID, tags); }
                        inline FrameH*      Read                          (int fileID = -1, int frameID = -1, const char * tags = "*"  ) { return Read(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID, tags); }
                        inline FrameH*      Read     (TFrameRHead *rHead,  int fileID,      int frameID,      std::vector<TString> tags) { return Read(rHead ? rHead->GetFile() : NULL, fileID, frameID, tags); }
                        inline FrameH*      Read                          (int fileID,      int frameID,      std::vector<TString> tags) { return Read(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID, tags); }
                        inline FrEndOfFile* ReadEOF  (TFrameRHead *rHead,  int fileID = -1) { return ReadEOF(rHead ? rHead->GetFile() : NULL, fileID); }
                        inline FrEndOfFile* ReadEOF                       (int fileID = -1) { return ReadEOF(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID); }

                        inline bool Find  (TFrameRHead *rHead, int fileID, int frameID) { return Find(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline bool Find  (                    int fileID, int frameID) { return Find(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }
                        inline void Free  (TFrameRHead *rHead, int fileID, int frameID) { return Free(rHead ? rHead->GetFile() : NULL, fileID, frameID); }
                        inline void Free  (                    int fileID, int frameID) { return Free(this->GetRHead(0) ? this->GetRHead(0)->GetFile() : NULL, fileID, frameID); }

                        int Write(FrFile *oFile, int fileID, int frameID, Option_t *option = "", std::vector<FrType> dataTypeList = {}, const char * tags = "*");
                        int Write(FrFile *oFile, int fileID, int frameID, Option_t *option, std::vector<FrType> dataTypeList, std::vector<TString> tags);

                        
                        FrameH* Reshape(FrameH* frame,                  int frameLength, Option_t *option = "");
                        FrameH* Reshape(FrameH* frame1, FrameH* frame2, int frameLength, Option_t *option = "");

                        int Trend(FrFile *oFile, std::vector<FrAdcData*> adc, int fs, Option_t *option = "");

                ClassDef(TFrameManager,1);
        };
}
